package com.bankmega.helloworld.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Objects;

@Service
public class HelloWorldService {
    private final Logger log = LoggerFactory.getLogger(HelloWorldService.class);

    public HashMap<String, Object> helloWorld(HashMap<String, Object> payload){
        return payload;
    }
}
