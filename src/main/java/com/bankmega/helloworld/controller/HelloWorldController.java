package com.bankmega.helloworld.controller;

import com.bankmega.helloworld.service.HelloWorldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class HelloWorldController {
    private Logger log = LoggerFactory.getLogger(HelloWorldService.class);

    private HelloWorldService helloWorldService;

    public HelloWorldController(HelloWorldService helloWorldService) {
        this.helloWorldService = helloWorldService;
    }

    @PostMapping("/helloworld")
    public ResponseEntity<HashMap<String, Object>> registrationCreatePin(@RequestBody HashMap<String, Object> payload) {
        log.info("REQUEST : {}", payload);
        HashMap<String, Object> response = helloWorldService.helloWorld(payload);
        log.info("RESPONSE : {}", payload);
        return ResponseEntity.ok(response);
    }
}
