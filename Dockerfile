FROM maven:3.8.6-eclipse-temurin-8 AS builder
WORKDIR /apps
COPY src ./src
COPY pom.xml .
RUN mvn package

FROM eclipse-temurin:8u362-b09-jre-alpine 
WORKDIR /apps
COPY --from=builder /apps/target/*.jar /apps/hello-world.jar
EXPOSE 45111
CMD [ "java", "-jar", "hello-world.jar" ]  
